/**
 * 
 * Estrutura principal do exercicio. Deve fazer uso das demais estruturas para
 * implementar uma lista ligada ordenada com base nas idades das pessoas
 * inseridas nela. Pessoas com idades menores no inicio da lista (mais proximos
 * da raiz).
 * 
 * O campo "inicio" define o inicio da lista ligada, sendo o ponto de acesso
 * para todos os demais itens da lista.
 * 
 * Os algoritmos desta lista NAO devem ser recursivos.
 * 
 */
public class ListaDinamicaOrdenada {

	/**
	 * NAO ALTERAR ESTA DEFINICAO
	 */
	No inicio = null;
	int qtdePessoas = 0;

	/**
	 * 
	 * Adiciona uma nova pessoa na posicao correta da lista, considerando que
	 * esta deve estar ordenada por idade (pessoas mais novas no inicio da
	 * lista).
	 * 
	 * @param p
	 */

	boolean inserir(Pessoa p) {
		if (qtdePessoas == 0) { // verifica se � nulo
			No novoNulo = new No();
			novoNulo.informacao = p;
			novoNulo.proximo = inicio;
			inicio = novoNulo;
			qtdePessoas++;
			
		} else {
			No percorre = inicio;
			if (qtdePessoas == 1) { // condi��o pra ferificar se existe 1 elemento
				if (percorre.informacao.idade < p.idade) { 
					No novonoInseriDepois = new No();                  
					novonoInseriDepois.informacao = p;
					novonoInseriDepois.proximo = percorre.proximo;
					percorre.proximo = novonoInseriDepois;
					qtdePessoas++;
					percorre = null;
				} else {                   // se n�o(se for menor) adcionar no inicio
					No novonoInseriInicio = new No();
					novonoInseriInicio.informacao = p;
					novonoInseriInicio.proximo = percorre;
					inicio = novonoInseriInicio;
					qtdePessoas++;
					percorre = null;
				}
			} else { // se houver mas de um
				while (percorre.proximo != null) { 
					if (p.idade < percorre.informacao.idade) {
						No novoNoOcupaInicio = new No();
						novoNoOcupaInicio.informacao = p;
						novoNoOcupaInicio.proximo = percorre;//inseri novo no, no inicio (inserie antes de joao e maria)
						inicio = novoNoOcupaInicio;
						qtdePessoas++;
						percorre = null;
					} else {
						if (percorre.proximo != null  & p.idade > percorre.proximo.informacao.idade) {
							No novoNoOcupaMeio = new No();
							novoNoOcupaMeio.informacao = p;
							novoNoOcupaMeio.proximo = percorre.proximo;
							percorre = novoNoOcupaMeio; //alterei aki (percorre.proximo = novono;)
							qtdePessoas++; //inseri no meio entre joao e maria
							percorre = null;
						}
						else {
							if (p.idade > percorre.proximo.informacao.idade) {
								No novoNoFim = new No();
								novoNoFim.informacao = p;
								novoNoFim.proximo = percorre.proximo; 
								percorre.proximo = novoNoFim;
								novoNoFim.proximo = null;
								qtdePessoas++;
								percorre = null;
							}else{
								if (p.idade > percorre.proximo.informacao.idade) {
									percorre = percorre.proximo;
								}
							}
							} //fim else noFim
						return true;
					}
				} //fim while
			} //else mais de um
		} //fim percorre
		return false;
	} //fim inserir

	/**
	 * 
	 * Deve retornar a quantidade de pessoas existentes na lista neste momento.
	 * 
	 * @return
	 */
	int recuperarQuantidadePessoas() {
		int qtde = qtdePessoas;
		return qtde;
	}

	/**
	 * 
	 * Imprime os nomes das pessoas na lista, a partir da raiz da lista. O
	 * formato a ser seguido tem o padrao:
	 * 
	 * {nome1, nome2, ..., nomen}
	 * 
	 * Caso a lista esteja vazia no momento que este metodo foi invocado, deve
	 * imprimir {}.
	 * 
	 * @return
	 */
	String imprimir() {
		String imp = "";
		No percorre = inicio;
		if(percorre == null){
			return "{}";
		}else{
			if(percorre != null){
				while (percorre != null) {
					imp = imp + percorre.informacao.nome + ", ";
					percorre = percorre.proximo;
				}
			}
			return "{ " + imp + " }";}
	}

	/**
	 * 
	 * Recupera um objeto do tipo Pessoa que esteja na lista e que tenha o nome
	 * igual ao parametro passado. Caso o nome nao seja encontrado, retorna
	 * null.
	 * 
	 * @param nomeProcurado
	 * @return
	 */
	Pessoa recuperarPorNome(String nomeProcurado) {
		No percorre = inicio;
		while (percorre == null) {
			return null;
		}
		if (percorre.informacao.nome == nomeProcurado) {
		}
		return percorre.informacao;
	}

	/**
	 * 
	 * Tenta remover o noh indicado naquela posicao. Considere que o primeiro
	 * elemento estah na posica zero da lista. Caso a posicao indicada nao
	 * exista na lista, nao faca nada.
	 * 
	 * @param posicaoParaRemover
	 */
	boolean remover(int posicaoParaRemover) {
	/*	No percorre = inicio;
		int numRemove = 0;
		while(percorre.proximo != null){
			if(numRemove == posicaoParaRemover){
				
			}
		}*/
		return false;
	 
}
	/**
	 * 
	 * Calcula a media das idades de todas as pessoas na lista e retorna esse
	 * valor.
	 * 
	 * @return
	 */
	double calcularMediaIdades() {
		No percorre = inicio;
     	int i = 0;
		double somaIdades = 0;
		double media = 0;
		while(percorre.proximo != null){
			somaIdades += percorre.informacao.idade;
			i += 1;
		}
		media = somaIdades/i;
		return media;
	}

	/**
	 * 
	 * Considerando as pessoas na lista ligada, retorna todos os nomes das
	 * pessoas num vetor. Se a lista estiver vazia, retorna null.
	 * 
	 * @return
	 */
	String[] recuperarNomes() {
		String [] nomesPessoas = new String [qtdePessoas];
		No percorre = inicio;
		int i = 0;
		while(percorre.proximo != null){
			nomesPessoas[i] = percorre.informacao.nome;
			i++;
		}
	return nomesPessoas;
	}
}
